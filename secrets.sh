#!/bin/bash -e


function validate_secret {
    if docker secret ls | grep -q "$1"; then
        echo " $1 exists, will not be re-adding to docker secrets."
        echo "ToDo: Clean up this code."
        return 0
    else
        echo "Adding Secret"
        echo $2 | docker secret create $1 -
        echo "$1 has been added to docker secrets"
        validate_secret
    fi
}

export -f validate_secret

cat .secretlist | \
    parallel \
        -j $(nproc) \
        """
        validate_secret {} $"{}"
        """
